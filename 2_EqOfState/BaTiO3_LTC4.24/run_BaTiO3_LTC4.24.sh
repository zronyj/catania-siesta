#!/bin/bash
#SBATCH --job-name SIESTA
#SBATCH -N 1
#SBATCH --ntasks-per-node=16
#SBATCH --cpus-per-task=1
#SBATCH --time=30:00
#SBATCH --account=tra22_esdmm_0
#SBATCH --partition=m100_usr_prod
#SBATCH --qos=normal
#SBATCH --output=output.LTC4.24.siesta

module load profile/chem-phys
module load autoload siesta/4.1-b4
export OMP_NUM_THREADS=1

runsiesta=$(which siesta)

cmd="$runsiesta < BaTiO3_LTC4.24.fdf"

# Run SIESTA using MPI with n = rank
mpirun -n 16 $cmd